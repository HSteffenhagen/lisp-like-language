#include <stddef.h>
#include <stdio.h>

typedef struct sexpr sexpr;

typedef double sexpr_number;

typedef struct sexpr_string sexpr_string;
struct sexpr_string {
  char const *begin;
  char const *end;
};

struct sexpr_identifier {
  char const *begin;
  char const *end;
};

typedef struct sexpr_list sexpr_list;
struct sexpr_list {
  size_t length;
  sexpr * elements[];
};

typedef enum sexpr_type sexpr_type;

enum sexpr_type {
  SEXPR_NIL,
  SEXPR_NUMBER,
  SEXPR_STRING,
  SEXPR_IDENTIFIER,
  SEXPR_LIST
};

inline char const* sexpr_type_to_string(sexpr_type type) {
  switch(type) {
    case SEXPR_NIL: return "nil";
    case SEXPR_NUMBER: return "number";
    case SEXPR_STRING: return "string";
    case SEXPR_IDENTIFIER: return "identifier";
    case SEXPR_LIST: return "list";
  }
}

struct sexpr {
  sexpr_type type;
  union {
    sexpr_number number;
    sexpr_string string;
    sexpr_identifier identifier;
    sexpr_list list;
  } value;
};


typedef double sexpr_token_number;
typedef struct sexpr_token_string sexpr_token_string;
struct sexpr_token_string {
  char *begin;
  char *end;
};
typedef struct sexpr_token_identifier sexpr_token_identifier;
struct sexpr_token_identifier {
  char *begin;
  char *end;
};

typedef enum sexpr_token_type sexpr_token_type;
enum sexpr_token_type {
  SEXPR_TOKEN_NIL,
  SEXPR_TOKEN_NUMBER,
  SEXPR_TOKEN_IDENTIFIER,
  SEXPR_TOKEN_STRING,
  SEXPR_TOKEN_LPAREN,
  SEXPR_TOKEN_RPAREN
};

typedef struct sexpr_token sexpr_token;
struct sexpr_token {
  sexpr_token_type type;
  union {
    sexpr_token_number;
    sexpr_token_string;
    sexpr_token_identifier;
  } value;
};

typedef void(*accept_sexpr_token_callback)(sexpr_token token, void *closure);

void untokenize(sexpr *s, accept_sexpr_token_callback callback);
